import React, {Component, PureComponent} from 'react';
import PropTypes from 'prop-types';
import CommentList from './commentList';

class Article extends Component{
    static propTypes = {
        article: PropTypes.shape({
            id: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired
        }).isRequired,
        isOpen: PropTypes.bool,
        toggleOpen: PropTypes.func
    }

    state = {
        updateIndex: 0
    }

    // partial update component
    // shouldComponentUpdate(nextProps, nextState){
    //     return nextProps.isOpen !== this.props.isOpen
    // }

    render(){
        const {article, isOpen, toggleOpen} = this.props;
        console.log('render');
        return(
            <div className="wrap-article" ref={this.setContainerRef}>
                <h3>{article.title}</h3>
                <button onClick={toggleOpen} className='button'>
                    {isOpen ? 'close' : 'open'}
                </button>
                {this.getBody()}
            </div>
        )
    }

    setContainerRef = ref => {
        this.container = ref;
        console.log('ref - ', ref);
    }

    getBody(){
        const {article, isOpen} = this.props;
        if(!isOpen) return null;
        return(
            <article>
                {article.text}
                <button onClick={()=> this.setState({updateIndex: this.state.updateIndex + 1})} className='button'>update</button>
                <CommentList comments = {article.comments} key={this.state.updateIndex}/>
            </article>
        )
    }
}

export default Article;

