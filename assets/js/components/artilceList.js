import React from 'react';
import PropTypes from 'prop-types';
import Article from './article';
import UserForm from './userForm';
import UserDayPicker from './userDayPicker';
import accordion from '../decorators/accordion';
import Counter from './counter';

function ArticleList({articles = [], openArticleId, toggleOpenArticle}){
    const articleElements = articles.map(article => <li key={article.id}>
        <Article
            article={article}
            isOpen = {article.id === openArticleId}
            toggleOpen = {toggleOpenArticle(article.id)}
        />
    </li>);

    return (
        <div>
            <Counter />
            <UserForm articles={articles}/>
            <UserDayPicker />
            <ul className='wrap-article-list no-bullet'>
                {articleElements}
            </ul>
        </div>
    )
}

ArticleList.propTypes = {
    articles: PropTypes.array.isRequired,
    openArticleId: PropTypes.string,
    toggleOpenArticle: PropTypes.func
}

export default accordion(ArticleList);