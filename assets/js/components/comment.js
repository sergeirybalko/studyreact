import React from 'react';
import PropTypes from 'prop-types';

export default function Comment({comment}) {
    return(
        <div className='wrap-comment'>
            <p>{comment.text} <strong> by {comment.user}</strong></p>
        </div>
    )
}

Comment.propTypes = {
    comment: PropTypes.shape({
        text: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired
    })
}