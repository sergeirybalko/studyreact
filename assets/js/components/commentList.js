import React from 'react';
import Comment from './comment';
import CommentListForm from './commentListForm';
import toggleOpen from '../decorators/toggleOpen';

function CommentList({comments = [], isOpen, toggleOpen}){

    const text = isOpen ? 'close comments' : 'open comments',
          btn =  comments.length ? <button onClick={toggleOpen} className='button warning'>{text}</button> : null;
    return(
        <div className='wrap-comment'>
            {btn}
            {getBody()}
        </div>
    )

    function getBody(){
        if(!isOpen) return null;
        if(!comments.length) return <p>No comments yet</p>

        return(
            <div className='wrap-list-comments'>
            <CommentListForm />
                <ul className="list-comments">
                    {comments.map(comment => <li key={comment.id}><Comment comment = {comment}/></li>)}
                </ul>
            </div>
        )
    }

}

export default toggleOpen(CommentList);