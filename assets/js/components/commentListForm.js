import React, {Component} from 'react';

export default class CommentListForm extends Component {
    constructor(props){
        super(props);
        this.limits = {
            userName: {
                min: 5,
                max: 15
            },
            userText: {
                min: 20,
                max: 50
            }
        }
    }
    state = {
        userName: '',
        userText: ''
    }
    render(){
        const {userName, userText} = this.state;
        return(
            <div className='wrap-comment-list-form'>
                <form>
                    <ul className='no-bullet'>
                        <li>
                            <label htmlFor='userName'>UserName</label>
                            <input id='userName' type="text" value={userName} onChange={this.handleChange('userName')} className={this.getClassName('userName')} />
                        </li>
                        <li>
                            <label htmlFor='userText'>UserText</label>
                            <textarea id="userText" value={userText} onChange={this.handleChange('userText')} className={this.getClassName('userText')}></textarea>
                        </li>
                    </ul>

                </form>
            </div>
        )
    }
    getClassName = type => this.state[type].length && this.state[type].length < this.limits[type].min ||
                           this.state[type].length && this.state[type].length > this.limits[type].max ? 'error' : '';
    handleChange = type => ev => {
        const {value} = ev.target;
        if(value.length > this.limits[type].max) return;
        this.setState({[type]: value})
    }
}