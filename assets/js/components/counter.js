import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {increment} from '../ac';

class Counter extends Component{
    static propTypes = {
        counter: PropTypes.number,
        increment: PropTypes.func
    }
    render(){
        return(
            <div>
                <h2 onClick={this.handleIncrement}>{this.props.counter}</h2>
            </div>
        )
    }
    handleIncrement = () => {
        const {increment} = this.props;
        increment();
    }
}

const decorator = connect(state => ({
    counter: state.count
}), {increment});

export default decorator(Counter);