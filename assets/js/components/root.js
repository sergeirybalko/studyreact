import React from 'react';
import PropTypes from 'prop-types';
import {Provider} from 'react-redux';
import ArticleList from './artilceList';
import store from '../sotre';

function Root(props) {
    return (
        <Provider store={store}>
            <ArticleList {...props}/>
        </Provider>
    )
}

Root.propTypes = {

}

export default Root;