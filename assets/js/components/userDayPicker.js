import React, {Component} from 'react';
import DayPicker, {DateUtils} from 'react-day-picker';

export default class UserDayPicker extends Component{
    static defaultProps = {
        numberOfMonths: 1
    }
    state = {
        from: undefined,
        to: undefined
    }
    render(){
        const {from, to} = this.state,
              modifiers = {start: from, end: to};
        return(
            <div className='wrap-day-picker'>
                {this.handleMessage()}
                <DayPicker className='Selectable' numberOfMonths={this.props.numberOfMonths} selectedDays={[from, {from, to}]} modifiers={modifiers} onDayClick={this.handleDayClick}/>
            </div>
        )
    }
    handleMessage(){
        const {from, to} = this.state,
              toggleClass = (from && to) ? 'range-message' : 'range-message error';
        return(
            <p className={toggleClass}>
                {!from && !to && 'Please select the first day.'}
                {from && !to && 'Please select the last day.'}
                {from && to &&
                    `Selected from ${from.toLocaleDateString()} to ${to.toLocaleDateString()} `}
                {from && to &&
                    <button className='click button' onClick={this.handleResetClick}>Reset</button>}
            </p>
        )
    }
    handleDayClick = day => {
        const range = DateUtils.addDayToRange(day, this.state);
        this.setState(range);
    }
    handleResetClick = () => this.setState({from: undefined, to: undefined})
}