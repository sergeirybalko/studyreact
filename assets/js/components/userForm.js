import React, {Component} from 'react';
import Select from 'react-select';

export default class UserForm extends Component {
    state = {
        selection: null
    }
    render(){
        const options = this.props.articles.map(article => ({
            label: article.title,
            value: article.id
        }));
        return(
            <div className='wrap-user-form'>
                <Select options={options} value = {this.state.selection} onChange={this.changeSelection} multi />
            </div>
        )
    }
    changeSelection = selection => this.setState({selection})
}