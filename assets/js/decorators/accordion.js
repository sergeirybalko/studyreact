import React, {Component as ReactComponent} from 'react';
import PropTypes from 'prop-types';

export default OriginalComponent => class WrappedAccordionComponent extends ReactComponent{
    state = {
        openArticleId: null
    }

    render(){
        return <OriginalComponent {...this.props} openArticleId = {this.state.openArticleId} toggleOpenArticle = {this.toggleOpenArticle} />
    }

    toggleOpenArticle = openItemId => () => {
        this.setState({
            openArticleId: openItemId === this.state.openArticleId ? null : openItemId
        })
    }
}