import React, {Component as ReactComponent} from 'react';
export default OriginalComponent => class WrappedComponent extends ReactComponent {
    state = {
        isOpen: true
    }
    // componentDidMount(){
    //     console.log('mounting');
    // }
    // componentDidUpdate(){
    //     console.log('update');
    // }
    render(){
        return <OriginalComponent {...this.props} {...this.state} toggleOpen = {this.toggleOpen} />
    }
    toggleOpen = () =>{
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
}