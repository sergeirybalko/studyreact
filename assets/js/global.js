import React from 'react';
import {render} from 'react-dom';
import {articles} from './data/fixtures';
import Root from './components/root';

render(
    <Root articles={articles}/>,
    document.getElementById('app')
);